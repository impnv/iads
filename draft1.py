# -*- coding: utf-8 -*-
"""
Created on Sat Oct 31 13:25:13 2020

@author: Ahmed
"""

def path cost(c, s1, a, s2):

    c1=0
    c2=0
    for i in range (1,len(s2[0])):
        c2+= (s2[0][i])*(s2.[0][i])
        c1+= (s1[0][i])*(s1[0][i]) 
    
    return(c+(c2-c1))



def load(f):
    L=f.readlines()
    
    for line in L:
        doc_code=[]
        labels=[[] for i in range(3)]
        patients=[[] for i in range(3)]
        ch=line.replace("\n","")
        if (ch==""):
                continue
        
        ch1=ch.split()
        if (ch[:2]=="MD"):
                
                doc_code.append(ch1[1])
                self.efficient_rates.append(ch1[2])
        
        if (ch[:2]=="PL"):
                labels[0].append(ch1[1])
                labels[1].append(ch1[2])
                labels[2].append(ch1[3])
        
        if (ch[:2]=="P "):
                
                patients[0].append(ch1[1])
                patients[1].append(ch1[2])
                patients[2].append(ch1[3])
        
    return(doc_code,labels,patients)
