# Artificial Intelligence and Decision Systems - Mini-Project 1

## How to use git

1. Go to **Settings** -> **SSH Keys** -> **Generate One**. Follow the directions provided to create and add an SSH Key.
2. Clone this repository. 
    -To do so go to **Clone** -> _Clone with SSH_ -> **Copy URL**
    - Open a terminal
    - Type the following command:
        ```
        git clone <paste the URL thet you previously copied>
        ```
3. Navigate onto the created folder
    ```
    cd occupancy-grid-mapping-socrob/
    ```
4. Create a branch for you to work on
    ```
    git checkout -b <Name of the branch>
    ```
5. Navigate between branches including the master
    ```
    git checkout <Name of the branch>
    ```
    - keep in mind that the master branch is supose to represent the last working and tested version of your code
6. To get the most updated version of the branch use:
    ```
    git pull
    ```
7. To get your new code up in the web you need to:
    ```
    git add <name of the file you changed>
    ```
    - repeat for every file you changed
    ```
    git commit -m <menssage>
    ```
    - in the message please write something that allows you to know what changes you made. One wonderfull thing about git is that it keeps a log of everything you do and allows you to go back to any step you have taken so choose your message wisely!
    ```
    git push
    ```
8. If you are unsure about how much you have changed you can just ask the git
    ```
    git status
    ```
    - this will display in green the changes you made and that you already added and in red the changes you made that are not added
9. To merge your code with one of your colegues
    ```
    git merge <name of the branch>
    ```
    - please do not do this alone (at least for the first endevor ask ines to help you :) )