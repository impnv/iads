### Template ###

import search 
import itertools
import math

class PDMAProblem(search.Problem):

    def __init__(self):
        #define o estado inicial
        
        #md, patient  = self.load()
        
        #((code1. code2), (efficiency1, efficiency2))
        self.md = []
        #[[patient1, patient2], [current_wait_t1, current_wait_t2], [max_wait_t1, max_wait_t2], [consult_t1, consult_t2]]
        self.patient = []

        #self.initial = (tuple(patient[1]),tuple(patient[3]))
        
        self.initial = []

        self.timestep = 5
        self.branching = []
        self.goal = None
    
    def actions(self, state):
        """

        Parameters
        ----------
        state : a stete of the problem
        
        Returns
        -------
        Returns a list of actions applicable to state, each
        actiion is a dictionary that translates the assignemtns. 
        The keys are the doctors and the values are the patients assign to each doctor
        action: {m1 : p2, m2: p3...}
        """
               #print(state)
        max_waiting_times = self.patient[2]
        waiting_times = list(state[0]).copy()
        left_consultation_time = list(state[1]).copy()
        patients = []
        patients_with_max_wait = []
        n_patients = len(max_waiting_times)
        n_mds = len(self.md[1])
        patients_permutations = []
        
        #time0 = time.time()
        #define patiante array to be permutated
        for i in range(n_patients):
            #if the patiente still needs consultation
            if left_consultation_time[i] > 0:  
                #if the patient can not wait any longer     
                if waiting_times[i] >= max_waiting_times[i]:
                    patients_with_max_wait.append(i+1)
                else: 
                    patients.append(i+1)
        #time1 = time.time()
        #print('1',time1 - time0)
        if len(patients_with_max_wait) > n_mds:
            return [[]]
        else:

            #check if we need to add empthy patients and how much            
            patients_to_be_attended = len(patients_with_max_wait) + len(patients)
            if(patients_to_be_attended < n_mds):
                for i in range(n_mds - patients_to_be_attended):
                    patients.append(0)
  
            #time2 = time.time()
            #print('2', time2 - time1)
            #get every possible combination of patients to be assigned not including the one that really have to be assinged.
            patients_permutations = [list(x) for x in itertools.permutations(patients, n_mds - len(patients_with_max_wait))]
        
            #remove unecessary patient combinations
            if(patients_to_be_attended < n_mds):
                aux = len(patients_permutations)
                i = 0
                j = 0
                while i < aux:
                    if 0 in patients_permutations[i]:
                        j = i+1
                        while j < aux:
                            #remove duplicate possibilities that arrived from having repeated patients (empty)
                            if (patients_permutations[i] == patients_permutations[j]):
                                patients_permutations.pop(j)
                                aux = aux - 1
                            else:
                                j = j + 1
                        j = 0
                    i = i + 1
            
            #time3 = time.time()
            #print('3', time3 - time2)
            aux = len(patients_permutations)

            #every patiente with max wait time has to be added to the created actions in every possible location in the vector
            for p in patients_with_max_wait:
                while aux > 0:
                    for j in range(1+len(patients_permutations[0])):
                        new_permutation = patients_permutations[0][:j] + [p] + patients_permutations[0][j:]
                        patients_permutations.append(new_permutation)
                    patients_permutations.pop(0)
                    aux = aux - 1
                aux = len(patients_permutations)

            self.branching.append(len(patients_permutations))
            return patients_permutations


        

    def result(self, state, action):
        """
        

        Parameters
        ----------
        state : a state of the problem
        action : a dictionary of assignments, keys are the doctors and
        the values are the assign patient

        Returns
        -------
        Returns a new state given the state and the action
        """
        rates = self.md[1]
        timestep = 5#self.timestep
        
        state_wating_times = state[0]
        state_left_times = state[1]
        n_pacientes = len(state_wating_times)
        
        new_wating_times = list(state[0]).copy()
        new_left_times = list(state[1]).copy()
        
        
        if len(action) != 0:

            for i in range(n_pacientes):
                if (i+1) in action and state[1][i]>0:
                    new_left_times[i] -= timestep*rates[action.index(i+1)]
                if (i+1) not in action and state[1][i]>0:
                    new_wating_times[i] += timestep
           
        return (tuple(new_wating_times), tuple(new_left_times))


        
    def goal_test(self, state):
        """
        

        Parameters
        ----------
        state : a state of the problem

        Returns
        -------
        Returns True if the state is a goal state, False Otherwise
        """
        
        state_left_times = state[1]
        return all([x <= 0 for x in state_left_times])

        
    def path_cost(self, c, state1, action, state2):
        """
        

        Parameters
        ----------
        c : path cost of state state1
        state1 : a state of the problem
        action : an action
        state2 : a state from the problem reached from state1 by applying the action a

        Returns
        -------
        Returns the path cost form state1 to state2

        """
        
        c1=0
        c2=0
        for i in range (len(state2[0])):
            c2+= (state2[0][i])*(state2[0][i])
            #c1+= (state1[0][i])*(state1[0][i]) 
        return ((c2-c))

        
    def heuristic(self, node):
        # note: if needed
        """
        

        Parameters
        ----------
        node : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        
        #new
        state = node.state
        n_mds = len(self.md[0])
        n_patients = len(state[0])
        n_patients_that_have_been_attended = state[1].count(0)

        
        
        #return sum(state_left_times)
        return ((n_patients - n_patients_that_have_been_attended - n_mds)*self.timestep)
        #return sum(state_left_times)/(sum(rates)*self.timestep)
        #return sum( math.sqrt(x) for x in state_left_times)
        
        

    

        
    def load(self, fh):
        # note: fh is an opened file object
        # note: self.initial may also be initialized here
        file_object = open(fh, 'r') 
        
        L = file_object.readlines()

        doc = [[] for i in range(2)]
        labels=[[] for i in range(3)]
        patients=[[] for i in range(4)]
        for line in L:
            ch=line.replace("\n","")
            if (ch==""):
                continue
            
            ch1=ch.split()
            if (ch[:2]=="MD"):
                doc[0].append(ch1[1])
                doc[1].append(float(ch1[2]))
            
            if (ch[:2]=="PL"):
                labels[0].append(ch1[1])
                labels[1].append(int(ch1[2]))
                labels[2].append(int(ch1[3]))
            
            if (ch[:2]=="P "):
                patients[0].append(ch1[1])
                patients[1].append(int(ch1[2]))
                for i in range(len(labels[0])):
                    if(ch1[3] == labels[0][i]):
                        patients[2].append(labels[1][i])
                        patients[3].append(labels[2][i])
                        
        
        
        self.md = doc
        #[[patient1, patient2], [current_wait_t1, current_wait_t2], [max_wait_t1, max_wait_t2], [consult_t1, consult_t2]]
        self.patient = patients

        self.initial = (tuple(patients[1]),tuple(patients[3]))
        self.branching = []

        #return(doc,patients)

    
    def save(self, fh):
        # note: fh is an opened file object
        
        file_object = open(fh, 'w') 
        
        if self.goal == None:
            file_object.write('No Solution')
            
        else:
            
            #initializing the dictionary of doctors assignments:
            solution = self.goal.solution()    
            doctor_assignemnts = {}
            
            for doc in self.md[0]:
                doctor_assignemnts[doc]=[]
            
            
            for action in solution:
                for i in range(len(action)):
                    doctor_assignemnts[self.md[0][i]].append(self.patient[0][action[i] - 1])
            
            
            for doc in doctor_assignemnts.keys():
                file_object.write('\n MD '+ doc +' ')
                for pac in doctor_assignemnts[doc]:
                    file_object.write(pac +' ')

        
        

    
    def search(self):
        # note: this function will compute the solution
        self.goal= search.astar_search(self, h=self.heuristic)
        
        if self.goal == None:
            return False
        else:
            print('depth',self.goal.depth)
            print('branching', sum(self.branching)/len(self.branching))
            print('number of nodes', sum(self.branching))
            return True
            

        
