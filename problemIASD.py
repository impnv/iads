# -*- coding: utf-8 -*-
"""
Created on Sat Oct 31 11:09:01 2020

@author: Regina.Duarte
"""

##state: ((wt1, wt2), (tleft1 , tleft2))
##action: {m1:p1, m2: p2, ....}

from search import Problem, SimpleProblemSolvingAgentProgram

class PMDAProblem(Problem):
    
    def __init__(self):
        md, patient  = self.load()

        #((code1. code2), (efficiency1, efficiency2))
        self.md = md
        #[[patient1, patient2], [current_wait_t1, current_wait_t2], [max_wait_t1, max_wait_t2], [consult_t1, consult_t2]]
        self.patient = patient

        self.initial = (tuple(patient[1]), tuple(patient[3]))

        self.timestep = 5

    
    
    def actions(self, state):
        """

        Parameters
        ----------
        state : a stete of the problem
        
        Returns
        -------
        Returns a list of actions applicable to state, each
        actiion is a dictionary that translates the assignemtns. 
        The keys are the doctors and the values are the patients assign to each doctor
        action: {m1 : p2, m2: p3...}
        """
        #implement heap permutation algorithm to get every permutation possible of patients
        def heap_permutation(size_a, a):    
            if size_a == 1:
                yield a
            
            else:
                # Recursively call once for each size_a
                for i in range(0, size_a):
                    perms = heap_permutation(size_a - 1, a)
                    for p in perms:
                        yield [*p]
                    # avoid swap when i==size_a - 1
                    if i < (size_a - 1):
                        # swap choice dependent on parity of size_a
                        if (size_a % 2) == 0 :
                            a[i], a[size_a - 1] = a[size_a - 1], a[i]
                        else:
                        a[0], a[size_a - 1] = a[size_a - 1], a[0]
        
        max_waiting_times = self.patient[2]
        n_patients = len(state[0])
        patients = []
        n_mds = len(self.md[1])
        patients_permutations = []
        action_set = {}
        
        #define patiante array to be permutated
        for i in range(n_mds):
            patients.append(0)
        for i in range(1, n_patients + 1):
            patients.append(i)

        #set the number of patientes for the real number plus the number of mds to account for blank spots
        n_patients = len(patients)
        #get every possible combination of patients to be assigned 
        for i in heap_permutation(n_patients, patients):
            patients_permutations.append(i)  
    
        #remove unecessary patient combinations
        aux = len(patients_permutations)
        i = 0
        j = 0
        while i < aux:
            while j < aux:
                #remove duplicate possibilities that arrived from cutting the permutation combinations
                if (i != j) and (patients_permutations[i][:n_mds] == patients_permutations[j][:n_mds]):
                    patients_permutations.pop(j)
                    aux = aux - 1
                else:
                    j = j + 1
            patients_permutations[i][n_mds:] = []
            j = 0
            i = i + 1

        #check if there are any patients that can not increase their waiting time pass this step
        j = 0
        aux = len(patients_permutations)
        # start this loop at n_mds to not look in to the unexisting waiting times of the empthy patients
        for i in range(n_mds, n_patients):
            if((state[0][i - n_mds]) == max_waiting_times[i - n_mds]):
                #we have found a patient that can not wait any longer and has to be a part of the actions
                while j < aux:
                    if i not in patients_permutations[j]:
                        patients_permutations.pop(j)
                        aux = aux - 1
                    else:
                        j = j + 1
                j = 0

        #write combination in to a dictionary and return it
        for i in range(len(patients_permutations)):
            for j in range(n_mds):
                if(patients_permutations[i][j] == 0):
                    action_set[self.md[0][j]] = 'empty'
                else: 
                action_set[self.md[0][j]] = self.patient[0][patients_permutations[i][j] - 1]

        return action_set

    
    def result(self, state, action):
        """
        

        Parameters
        ----------
        state : a state of the problem
        action : a dictionary of assignments, keys are the doctors and
        the values are the assign patient

        Returns
        -------
        Returns a new state given the state and the action
        """
        
        rates = self.md[1]
        timestep = self.timestep
        
        state_wating_times = state[0]
        state_left_times = state[1]
        n_pacientes = len(state_wating_times)
        
        new_wating_times =state_wating_times
        new_left_times = state_left_times

        
        for i in range(n_pacientes):
            
            if self.patient[0][i] not in action.values() and state_left_times[i]!=0:
                            
                new_wating_times[i] += timestep
                
        for j in range(len(rates)): 

            if self.md[0][j] in action.keys():

                new_left_times.append -= timestep*rates[j]
                
                
        return (tuple(new_wating_times), tuple(new_left_times))
    
    
    def goal_test(self, state):
        """
        

        Parameters
        ----------
        state : a state of the problem

        Returns
        -------
        Returns True if the state is a goal state, False Otherwise
        """
        
        state_left_times = state[1]
        return all([x == 0 for x in state_left_times])
    
    
    def path_cost(self, c, state1, a, state2):
        """
        

        Parameters
        ----------
        c : path cost of state state1
        state1 : a state of the problem
        action : an action
        state2 : a state from the problem reached from state1 by applying the action a

        Returns
        -------
        Returns the path cost form state1 to state2

        """

        c1=0
        c2=0
        for i in range (1,len(s2[0])):
            c2+= (state2[0][i])*(state2[0][i])
            c1+= (state1[0][i])*(state1[0][i]) 
    
        return(c+(c2-c1))
        
    
    def heuristic(self, node):
        """
        

        Parameters
        ----------
        node : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        state = node.state
        state_left_times = state[1]
        return sum(state_left_times)
    
    def search(self):
        problemsolving = SimpleProblemSolvingAgentProgram()
        
        node = problemsolving.aster_search(self, h=self.heuristic())
        
        if node == None:
            return False
        else:
            return True
        
        ## is this the reasoning or we have to verify if the node is a valid state?
        
    
    def load(self, file):

        L=file.readlines()
    
        doc = [[] for i in range(2)]
        labels=[[] for i in range(3)]
        patients=[[] for i in range(4)]
        for line in L:
            ch=line.replace("\n","")
            if (ch==""):
                continue
            
            ch1=ch.split()
            if (ch[:2]=="MD"):
                doc[0].append(ch1[1])
                doc[1].append(int(ch1[2]))
            
            if (ch[:2]=="PL"):
                labels[0].append(ch1[1])
                labels[1].append(int(ch1[2]))
                labels[2].append(int(ch1[3]))
            
            if (ch[:2]=="P "):
                patients[0].append(ch1[1])
                patients[1].append(int(ch1[2]))
                for i in range(len(labels[0])):
                    if(ch1[3] == labels[0][i]):
                        patients[2].append(labels[1][i])
                        patients[3].append(labels[2][i])

            
            
        return(doc,patients)
            
    
    def save(self, file):
        problemsolving = SimpleProblemSolvingAgentProgram()
        
        node = problemsolving.aster_search(self, h=self.heuristic())
        
        solution = node.solution()
        #initializing the dictionary of doctors assignments:
            
        doctor_assignemnts = {}
        
        for doc in self.md[0]:
            doctor_assignemnts[doc]=[]
        
        
        for action in solution:
            for doc, pac in action.items():
                doctor_assignemnts[doc].append(pac)
        
        file_object = open('file', 'w')
        
        for doc in doctor_assignemnts.keys():
            file_object.write('\n MD '+ doc +' ')
            for pac in doctor_assignemnts[doc]:
                file_object.write(pac +' ')
        
                
            
        file_object.close()
            
            
                
        
        
    
    
    
